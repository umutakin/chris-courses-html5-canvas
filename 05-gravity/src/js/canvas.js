import utils from "./utils";

const canvas = document.querySelector("canvas");
const c = canvas.getContext("2d");

canvas.width = innerWidth;
canvas.height = innerHeight;

const mouse = {
    x: innerWidth / 2,
    y: innerHeight / 2,
};

const colors = ["#2185C5", "#7ECEFD", "#FFF6E5", "#FF7F66"];

var gravity = 1;
var friction_x = 0.79;
var friction_y = 0.79;

// Event Listeners

addEventListener("mousemove", (event) => {
    mouse.x = event.clientX;
    mouse.y = event.clientY;
});

addEventListener("resize", () => {
    canvas.width = innerWidth;
    canvas.height = innerHeight;

    init();
});

addEventListener("click", function () {
    init();
});

// Objects

class Ball {
    constructor(x, y, dx, dy, radius, color) {
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
        this.radius = radius;
        this.color = color;
    }

    draw() {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        c.fillStyle = this.color;
        c.fill();
        c.stroke();
        c.closePath();
    }

    update() {
        if (
            this.x + this.radius + this.dx > canvas.width ||
            this.x - this.radius <= 0
        ) {
            this.dx = -this.dx * friction_x;
        }

        if (this.y + this.radius + this.dy > canvas.height) {
            this.dy = -this.dy * friction_y;
        } else {
            this.dy += gravity;
        }

        this.x += this.dx;
        this.y += this.dy;
        this.draw();
    }
}

// Implementation

/** @type {Ball[]} */
var ballArray;
function init() {
    ballArray = [];

    for (let i = 0; i < 400; i++) {
        var radius = utils.randomIntFromRange(5, 20);
        var x = utils.randomIntFromRange(radius, canvas.width - radius);
        var y = utils.randomIntFromRange(0, canvas.height - radius);
        var dx = utils.randomIntFromRange(-2, 2);
        var dy = utils.randomIntFromRange(-2, 2);
        var color = utils.randomColor(colors);
        ballArray.push(new Ball(x, y, dx, dy, radius, color));
    }
}

// Animation Loop

function animate() {
    requestAnimationFrame(animate);

    c.clearRect(0, 0, canvas.width, canvas.height);

    for (let i = 0; i < ballArray.length; i++) {
        // ballArray[i].update();
        ballArray[i].update();
    }

    ball.update();
}

init();
animate();
