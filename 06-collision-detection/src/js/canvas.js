import utils from "./utils";

const canvas = document.querySelector("canvas");
const c = canvas.getContext("2d");

canvas.width = innerWidth;
canvas.height = innerHeight;

const mouse = {
    x: innerWidth / 2,
    y: innerHeight / 2,
};

const colors = ["#2185C5", "#7ECEFD", "#FFF6E5", "#FF7F66"];

// Event Listeners
addEventListener("mousemove", (event) => {
    mouse.x = event.clientX;
    mouse.y = event.clientY;
});

addEventListener("resize", () => {
    canvas.width = innerWidth;
    canvas.height = innerHeight;

    init();
});

// Objects

class Circle {
    /**
     * Create a circle.
     * @param {number} x - The x-coordinate of the center of the circle.
     * @param {number} y - The y-coordinate of the center of the circle.
     * @param {number} radius - The radius of the circle.
     * @param {string} color - The color of the circle.
     */
    constructor(x, y, radius, color) {
        /** @type {number} */
        this.x = x;
        /** @type {number} */
        this.y = y;
        /** @type {number} */
        this.radius = radius;
        /** @type {string} */
        this.color = color;
    }

    /**
     * Draw the circle on the canvas.
     * @param {CanvasRenderingContext2D} c - The canvas rendering context.
     */
    draw() {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        c.fillStyle = this.color;
        c.fill();
        c.closePath();
    }

    /**
     * Update the circle properties and re-draw it.
     * @param {CanvasRenderingContext2D} c - The canvas rendering context.
     */
    update() {
        this.draw();
    }
}

// Implementation
/** @type {Circle} - The first circle.  */
let circle1;
/** @type {Circle} - The second circle.   */
let circle2;

function init() {
    circle1 = new Circle(300, 300, 100, "black");
    circle2 = new Circle(undefined, undefined, 30, "red");
}

// Animation Loop
function animate() {
    requestAnimationFrame(animate);
    c.clearRect(0, 0, canvas.width, canvas.height);

    circle1.update();

    circle2.x = mouse.x;
    circle2.y = mouse.y;
    circle2.update();

    if (
        utils.distance(circle1.x, circle1.y, circle2.x, circle2.y) <
        circle1.radius + circle2.radius
    ) {
        circle1.color = "red";
    } else {
        circle1.color = "black";
    }
}

init();
animate();
