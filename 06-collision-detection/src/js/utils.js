function randomIntFromRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function randomColor(colors) {
    return colors[Math.floor(Math.random() * colors.length)];
}

/**
 * @param {number} x1 X coordinate of object 1
 * @param {number} y1 Y coordinate of object 1
 * @param {number} x2 X coordinate of object 2
 * @param {number} y2 Y coordinate of object 2
 * @returns {number} distance between two points (x1, y1) and (x2, y2) in pixels.}
 */
function distance(x1, y1, x2, y2) {
    const xDist = x2 - x1;
    const yDist = y2 - y1;

    return Math.sqrt(Math.pow(xDist, 2) + Math.pow(yDist, 2));
}

export default { randomIntFromRange, randomColor, distance };
