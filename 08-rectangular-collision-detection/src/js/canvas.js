import utils from "./utils";

const canvas = document.querySelector("canvas");
const c = canvas.getContext("2d");

canvas.width = innerWidth;
canvas.height = innerHeight;

const mouse = {
    x: innerWidth / 2,
    y: innerHeight / 2,
};

const colors = ["#2185C5", "#7ECEFD", "#FFF6E5", "#FF7F66"];

// Event Listeners
addEventListener("mousemove", (event) => {
    mouse.x = event.clientX;
    mouse.y = event.clientY;
});

addEventListener("resize", () => {
    canvas.width = innerWidth;
    canvas.height = innerHeight;

    init();
});

// Objects
class Rectangle {
    constructor(x, y, width, height, color) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        // this.radius = radius;
        this.color = color;
    }

    draw() {
        c.beginPath();
        // c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        c.rect(this.x, this.y, this.width, this.height);
        c.fillStyle = this.color;
        c.fill();
        c.closePath();
    }

    update() {
        this.draw();
    }
}

// Implementation
/** @type {Rectangle} */
let rect_red;
/** @type {Rectangle} */
let rect_blue;
function init() {
    rect_red = new Rectangle(mouse.x, mouse.y, 100, 100, "#E86262");
    rect_blue = new Rectangle(canvas.width / 2 - 50, canvas.height / 2 - 50, 100, 100, "#92ABEA");
}

// Animation Loop

function animate() {
    requestAnimationFrame(animate);
    c.fillStyle = "#1A1A23";
    c.fillRect(0, 0, canvas.width, canvas.height);

    if (
        mouse.x + 100 >= canvas.width / 2 - 50 &&
        mouse.x <= canvas.width / 2 - 50 + 100 &&
        mouse.y + 100 >= canvas.height / 2 - 50 &&
        mouse.y <= canvas.height / 2 - 50 + 100
    ) {
        console.log("colliding");
    }

    rect_red.x = mouse.x;
    rect_red.y = mouse.y;
    rect_red.update();
    rect_blue.update();
}

init();
animate();
