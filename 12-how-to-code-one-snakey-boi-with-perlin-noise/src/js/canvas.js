import { noise } from "@chriscourses/perlin-noise";

const canvas = document.querySelector("canvas");
const c = canvas.getContext("2d");

canvas.width = innerWidth;
canvas.height = innerHeight;

// Objects
class Circle {
    constructor(x, y, radius, color, offset) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.offset = offset;
    }

    draw() {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        c.fillStyle = this.color;
        c.fill();
        c.closePath();
    }

    update() {
        this.draw();
    }
}

const circles = [];

for (let i = 0; i < 360; i++) {
    circles.push(
        new Circle(canvas.width / 2, canvas.height / 2, 10, `hsl(${Math.random() * 360}, 50%, 50%)`, i * 0.01)
    );
}

let time = 0;

function animate() {
    requestAnimationFrame(animate);
    c.fillStyle = "rgba(0, 0, 0, 0.05)";
    c.fillRect(0, 0, canvas.width, canvas.height);

    circles.forEach((circle) => {
        circle.draw();
        circle.x = noise(time + circle.offset + 20) * canvas.width;
        circle.y = noise(time + circle.offset) * canvas.height;
        circle.radius += 0.01;
    });

    time += 0.005;
}

animate();
