const canvas = document.querySelector("canvas");
const c = canvas.getContext("2d");

canvas.width = innerWidth;
canvas.height = innerHeight;

const scoreEl = document.querySelector("#scoreEl");
const bigScoreEl = document.querySelector("#bigScoreEl");
const modalEl = document.querySelector("#modalEl");
const startGameBtn = document.querySelector("#startGameBtn");

class Player {
    constructor(x, y, radius, color) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
    }

    draw() {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        c.fillStyle = this.color;
        c.fill();
    }
}

class Projectile {
    constructor(x, y, radius, color, velocity) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.velocity = velocity;
    }

    draw() {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        c.fillStyle = this.color;
        c.fill();
    }

    update() {
        this.draw();
        this.x = this.x + this.velocity.x;
        this.y = this.y + this.velocity.y;
    }
}

class Enemy {
    constructor(x, y, radius, color, velocity) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.velocity = velocity;
    }

    draw() {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        c.fillStyle = this.color;
        c.fill();
    }

    update() {
        this.draw();
        this.x = this.x + this.velocity.x;
        this.y = this.y + this.velocity.y;
    }
}

const friction = 0.97;

class Particle {
    constructor(x, y, radius, color, velocity) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.velocity = velocity;
        this.alpha = 1;
    }

    draw() {
        c.save();
        c.globalAlpha = this.alpha;
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        c.fillStyle = this.color;
        c.fill();
        c.restore();
    }

    update() {
        this.draw();
        this.velocity.x *= friction;
        this.velocity.y *= friction;
        this.x = this.x + this.velocity.x;
        this.y = this.y + this.velocity.y;
        this.alpha -= 0.01;
    }
}

const x = canvas.width / 2;
const y = canvas.height / 2;

/** @type {Player} */
let player;

/** @type {Projectile[]} */
let projectiles;

/** @type {Enemy[]} */
let enemies;
let enemyInterval;

/** @type {Particle[]} */
let particles;

function init() {
    player = new Player(x, y, 10, "white");
    projectiles = [];
    enemies = [];
    particles = [];

    clearInterval(enemyInterval);

    score = 0;
    scoreEl.innerHTML = score;
    bigScoreEl.innerHTML = score;
}

function spawnEnemies() {
    enemyInterval = setInterval(() => {
        const radius = Math.random() * (30 - 4) + 4;

        let x;
        let y;

        if (Math.random() < 0.5) {
            x = Math.random() < 0.5 ? 0 - radius : canvas.width + radius;
            y = Math.random() * canvas.height;
        } else {
            x = Math.random() * canvas.width;
            y = Math.random() < 0.5 ? 0 - radius : canvas.height + radius;
        }

        // const color = "orange";
        const color = `hsl(${Math.random() * 360}, 50%, 50%)`;

        const angle = Math.atan2(canvas.height / 2 - y, canvas.width / 2 - x);
        const velocity = { x: Math.cos(angle), y: Math.sin(angle) };

        enemies.push(new Enemy(x, y, radius, color, velocity));
    }, 3000);
}

let score = 0;
let animationId;
function animate() {
    animationId = requestAnimationFrame(animate);

    c.fillStyle = "rgba(0, 0, 0, 0.1)";
    c.fillRect(0, 0, canvas.width, canvas.height);

    player.draw();

    particles.forEach((particle, particleIndex) => {
        if (particle.alpha <= 0) {
            setTimeout(() => {
                particles.splice(particleIndex, 1);
            });
        } else {
            particle.update();
        }
    });

    projectiles.forEach((projectile, projectileIndex) => {
        projectile.update();

        // remove off screen projectiles
        if (
            projectile.x + projectile.radius < 0 ||
            projectile.x - projectile.radius > canvas.width ||
            projectile.y + projectile.radius < 0 ||
            projectile.y - projectile.radius > canvas.height
        ) {
            setTimeout(() => {
                projectiles.splice(projectileIndex, 1);
            });
        }
    });

    enemies.forEach((enemy, enemyIndex) => {
        enemy.update();

        projectiles.forEach((projectile, projectileIndex) => {
            const distanceOfProjectileAndEnemy = Math.hypot(projectile.x - enemy.x, projectile.y - enemy.y);

            // projectile and enemy touch
            if (distanceOfProjectileAndEnemy - projectile.radius - enemy.radius < 1) {
                // create explosions
                for (i = 0; i < enemy.radius * 2; i++) {
                    particles.push(
                        new Particle(projectile.x, projectile.y, Math.random() * 2, enemy.color, {
                            x: (Math.random() - 0.5) * (Math.random() * 8),
                            y: (Math.random() - 0.5) * (Math.random() * 8),
                        })
                    );
                }

                if (enemy.radius - 10 > 5) {
                    // increase score
                    score += 100;
                    scoreEl.innerHTML = score;

                    gsap.to(enemy, { radius: enemy.radius - 10 });
                    setTimeout(() => {
                        projectiles.splice(projectileIndex, 1);
                    }, 0);
                } else {
                    // increase score
                    score += 250;
                    scoreEl.innerHTML = score;

                    setTimeout(() => {
                        enemies.splice(enemyIndex, 1);
                        projectiles.splice(projectileIndex, 1);
                    }, 0);
                }
            }
        });

        const distanceOfPlayerAndEnemy = Math.hypot(player.x - enemy.x, player.y - enemy.y);

        // end game
        if (distanceOfPlayerAndEnemy - enemy.radius - player.radius < 1) {
            cancelAnimationFrame(animationId);
            // clearInterval(enemyInterval);
            modalEl.style.display = "flex";
            bigScoreEl.innerHTML = score;
        }
    });
}

addEventListener("click", (event) => {
    // console.log("PROJECTILE COUNT", projectiles.length);
    const angle = Math.atan2(event.clientY - canvas.height / 2, event.clientX - canvas.width / 2);
    const velocity = { x: Math.cos(angle) * 5, y: Math.sin(angle) * 5 };
    projectiles.push(new Projectile(canvas.width / 2, canvas.height / 2, 5, "white", velocity));
});

startGameBtn.addEventListener("click", () => {
    init();

    animate();
    spawnEnemies();

    modalEl.style.display = "none";
});
