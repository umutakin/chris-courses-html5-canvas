import platform from "../img/platform.png";
import platformSmallTall from "../img/platformSmallTall.png";
import hills from "../img/hills.png";
import background from "../img/background.png";
import spriteRunLeft from "../img/spriteRunLeft.png";
import spriteRunRight from "../img/spriteRunRight.png";
import spriteStandLeft from "../img/spriteStandLeft.png";
import spriteStandRight from "../img/spriteStandRight.png";

const canvas = document.querySelector("canvas");

const c = canvas.getContext("2d");

canvas.width = 1280;
canvas.height = 720;

const gravity = 0.5;

class Player {
    constructor() {
        this.position = {
            x: 100,
            y: 100,
        };
        this.velocity = {
            x: 0,
            y: 1,
        };
        this.width = 66;
        this.height = 150;
        this.speed = 10;

        this.image = createImage(spriteStandRight);
        this.frames = 0;
        this.sprites = {
            stand: {
                right: createImage(spriteStandRight),
                left: createImage(spriteStandLeft),
                cropWidth: 177,
                width: 66,
            },
            run: {
                right: createImage(spriteRunRight),
                left: createImage(spriteRunLeft),
                cropWidth: 341,
                width: 127.875,
            },
        };
        this.currentSprite = this.sprites.stand.right;
        this.currentCropWidth = this.sprites.stand.cropWidth;
    }

    draw() {
        c.drawImage(
            this.currentSprite,
            this.currentCropWidth * this.frames,
            0,
            this.currentCropWidth,
            400,
            this.position.x,
            this.position.y,
            this.width,
            this.height
        );
    }

    update() {
        this.frames++;
        if (
            (this.currentSprite === this.sprites.stand.right || this.currentSprite === this.sprites.stand.left) &&
            this.frames > 59
        ) {
            this.frames = 0;
        } else if (
            (this.currentSprite === this.sprites.run.right || this.currentSprite === this.sprites.run.left) &&
            this.frames > 29
        ) {
            this.frames = 0;
        }
        this.draw();
        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;

        // Gravity
        if (this.position.y + this.height + this.velocity.y <= canvas.height) {
            this.velocity.y += gravity;
        }
    }
}

class Platform {
    constructor({ x, y, image }) {
        this.position = {
            x,
            y,
        };

        this.image = image;

        this.width = image.width;
        this.height = image.height;
    }

    draw() {
        c.drawImage(this.image, this.position.x, this.position.y);
    }
}

class GenericObject {
    constructor({ x, y, image }) {
        this.position = {
            x,
            y,
        };

        this.image = image;

        this.width = image.width;
        this.height = image.height;
    }

    draw() {
        c.drawImage(this.image, this.position.x, this.position.y);
    }
}

/**
 * @param {*} imageSrc
 * @returns { Image }
 */
function createImage(imageSrc) {
    const image = new Image();
    image.src = imageSrc;

    return image;
}

/** @type {Image}  */
let platformImage;
/** @type {Image} */
let platformSmallTallImage;
/** @type {Player} */
let player;
/** @type {Platform[]} */
let platforms;
/** @type {GenericObject[]}  */
let genericObjects;
/** @type {number} */
let scrollOffset;

function init() {
    platformImage = createImage(platform);
    platformSmallTallImage = createImage(platformSmallTall);

    scrollOffset = 0;

    player = new Player();
    platforms = [
        new Platform({
            x: platformImage.width * 5 + 300 - 3,
            y: canvas.height - platformSmallTallImage.height,
            image: platformSmallTallImage,
        }),
        new Platform({
            x: -1,
            y: canvas.height - platformImage.height,
            image: platformImage,
        }),
        new Platform({
            x: platformImage.width - 3,
            y: canvas.height - platformImage.height,
            image: platformImage,
        }),
        new Platform({
            x: platformImage.width * 2 + 100,
            y: canvas.height - platformImage.height,
            image: platformImage,
        }),
        new Platform({
            x: platformImage.width * 3 + 300,
            y: canvas.height - platformImage.height,
            image: platformImage,
        }),
        new Platform({
            x: platformImage.width * 4 + 300 - 3,
            y: canvas.height - platformImage.height,
            image: platformImage,
        }),
        new Platform({
            x: platformImage.width * 7,
            y: canvas.height - platformImage.height,
            image: platformImage,
        }),
    ];

    genericObjects = [
        new GenericObject({
            x: -1,
            y: -1,
            image: createImage(background),
        }),
        new GenericObject({
            x: 15,
            y: 15,
            image: createImage(hills),
        }),
    ];
}

let lastKey;
const keys = {
    right: {
        pressed: false,
    },
    left: {
        pressed: false,
    },
};

function animate() {
    requestAnimationFrame(animate);

    c.fillStyle = "white";
    c.fillRect(0, 0, canvas.width, canvas.height);

    genericObjects.forEach((genericObject) => {
        genericObject.draw();
    });

    platforms.forEach((platform) => {
        platform.draw();
    });
    player.update();

    if (keys.right.pressed && player.position.x < 400) {
        player.velocity.x = player.speed;
    } else if (
        (keys.left.pressed && player.position.x > 100) ||
        (keys.left.pressed && scrollOffset === 0 && player.position.x > 0)
    ) {
        player.velocity.x = -player.speed;
    } else {
        player.velocity.x = 0;

        if (keys.right.pressed) {
            scrollOffset += player.speed;
            platforms.forEach((platform) => {
                platform.position.x -= player.speed;
            });
            genericObjects.forEach((genericObject) => {
                genericObject.position.x -= player.speed * 0.66;
            });
        } else if (keys.left.pressed && scrollOffset > 0) {
            scrollOffset -= player.speed;
            platforms.forEach((platform) => {
                platform.position.x += player.speed;
            });
            genericObjects.forEach((genericObject) => {
                genericObject.position.x += player.speed * 0.66;
            });
        }
    }

    // platform collision detection
    platforms.forEach((platform) => {
        if (
            player.position.y + player.height <= platform.position.y &&
            player.position.y + player.height + player.velocity.y >= platform.position.y &&
            player.position.x + player.width >= platform.position.x &&
            player.position.x <= platform.position.x + platform.width
        ) {
            player.velocity.y = 0;
        }
    });

    console.log(lastKey);

    // sprite switching
    if (keys.right.pressed && lastKey === "right" && player.currentSprite !== player.sprites.run.right) {
        player.frames = 0;
        player.currentSprite = player.sprites.run.right;
        player.currentCropWidth = player.sprites.run.cropWidth;
        player.width = player.sprites.run.width;
    } else if (keys.left.pressed && lastKey === "left" && player.currentSprite !== player.sprites.run.left) {
        player.frames = 0;
        player.currentSprite = player.sprites.run.left;
        player.currentCropWidth = player.sprites.run.cropWidth;
        player.width = player.sprites.run.width;
    } else if (!keys.left.pressed && lastKey === "left" && player.currentSprite !== player.sprites.stand.left) {
        player.frames = 0;
        player.currentSprite = player.sprites.stand.left;
        player.currentCropWidth = player.sprites.stand.cropWidth;
        player.width = player.sprites.stand.width;
    } else if (!keys.right.pressed && lastKey === "right" && player.currentSprite !== player.sprites.stand.right) {
        player.frames = 0;
        player.currentSprite = player.sprites.stand.right;
        player.currentCropWidth = player.sprites.stand.cropWidth;
        player.width = player.sprites.stand.width;
    }
    // } else if (lastKey === "") {
    //     player.frames = 0;
    //     player.currentCropWidth = player.sprites.stand.cropWidth;
    //     player.width = player.sprites.stand.width;
    //     if (player.currentSprite === player.sprites.run.right) {
    //         player.currentSprite = player.sprites.stand.right;
    //     } else if (player.currentSprite === player.sprites.run.left) {
    //         player.currentSprite = player.sprites.stand.left;
    //     }
    // }
    // } else if (lastKey === "right" && player.velocity.x === 0) {
    //     player.frames = 0;
    //     player.currentSprite = player.sprites.stand.right;
    //     player.currentCropWidth = player.sprites.stand.cropWidth;
    //     player.width = player.sprites.stand.width;
    // } else if (lastKey === "left" && player.velocity.x === 0) {
    //     player.frames = 0;
    //     player.currentSprite = player.sprites.stand.left;
    //     player.currentCropWidth = player.sprites.stand.cropWidth;
    //     player.width = player.sprites.stand.width;
    // }

    // win condition
    if (scrollOffset > platformImage.width * 7 + 100) {
        console.log("YOU WIN");
    }

    // lose condition
    if (player.position.y > canvas.height) {
        console.log("YOU LOSE");
        init();
    }
}

init();
animate();

addEventListener("keydown", ({ key }) => {
    switch (key.toLowerCase()) {
        case "w":
            if (Math.abs(player.velocity.y) == 0) {
                player.velocity.y -= 15;
            }
            break;
        case "a":
            keys.left.pressed = true;
            lastKey = "left";
            // player.currentSprite = player.sprites.run.left;
            // player.currentCropWidth = player.sprites.run.cropWidth;
            // player.width = player.sprites.run.width;
            break;
        case "s":
            break;
        case "d":
            keys.right.pressed = true;
            lastKey = "right";
            // player.currentSprite = player.sprites.run.right;
            // player.currentCropWidth = player.sprites.run.cropWidth;
            // player.width = player.sprites.run.width;
            break;
    }
});

addEventListener("keyup", ({ key }) => {
    switch (key.toLowerCase()) {
        case "w":
            break;
        case "a":
            keys.left.pressed = false;
            // player.currentSprite = player.sprites.stand.left;
            // player.currentCropWidth = player.sprites.stand.cropWidth;
            // player.width = player.sprites.stand.width;
            break;
        case "s":
            break;
        case "d":
            keys.right.pressed = false;
            // player.currentSprite = player.sprites.stand.right;
            // player.currentCropWidth = player.sprites.stand.cropWidth;
            // player.width = player.sprites.stand.width;
            break;
    }
});
